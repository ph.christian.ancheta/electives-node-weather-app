# Weather App

## Setup
Run `npm install` on the root directory

## Run the Application
Run `node src/app.js` on the root directory

## Access the Web Application on Browser
`localhost:3000`
